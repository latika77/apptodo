package stepDefinations;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Tasks extends Abstractclass{
	WebDriver driver = getDriver();
	@When("^click on MyTasks link$")
	public void click_on_MyTasks_link() throws Throwable {
	    
				driver.findElement(By.xpath("//div[@class='navbar navbar-default']/following::a[1]")).click();
		
	}

	@When("^show the message with username$")
	public void show_the_message_with_username() throws Throwable {
	    driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//h1[contains(text(),'Latika ' ) ]")).getText();
	}
	@When("^create tasks and add it$")
	public void create_tasks_and_add_it() throws Throwable {
	    
		driver.findElement(By.xpath("//input[@id='new_task']")).sendKeys("Create a new task");
		driver.findElement(By.xpath("//span[@class='input-group-addon glyphicon glyphicon-plus']")).click();
	}
	@When("^create subtasks and add it$")
	public void create_subtasks_and_add_it() throws Throwable {
	    
		 driver.findElement(By.xpath("//div[contains(@class,'panel-heading')]/following::button[1]")).click();
		 driver.findElement(By.xpath("//input[@id='new_sub_task']")).sendKeys("New subtask");
		 driver.findElement(By.xpath("//input[@id='dueDate']"));
		 driver.findElement(By.xpath("//button[@id='add-subtask']")).click();
		 driver.findElement(By.xpath("//button[contains(text(),'Close')]")).click();
	 }
	@When("^click on Track a bug$")
	public void click_on_Track_a_bug() throws Throwable {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.findElement(By.xpath("//a[@href='/bugs']")).click();
	    


}
	
	@Then("^finish the project and close the browser$")
	public void finish_the_project_and_close_the_browser() throws Throwable {
	   
	    driver.close();
	}
}